// Taller de Ezequiel Cribioli, LU: 702/18

#include "Lista.h"
#include <cassert>

Lista::Lista() {
    // Completar
    primero = nullptr;
    ultimo = nullptr;
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

void Lista::Destructor() {
    Nodo* actual = primero;
    Nodo* proximo = primero;
    while(actual){
        proximo = actual->sig;
        delete actual;
        actual = proximo;
    }
}

Lista::~Lista() {
    // Completar
    Destructor();
}

Lista& Lista::operator=(const Lista& aCopiar) {
    // Completar
    Destructor();
    ultimo = nullptr;
    primero = nullptr;
    Nodo* copia = aCopiar.primero;
    while(copia){
        Nodo* nuevo = new Nodo(copia->val, ultimo, nullptr);
        if(ultimo) ultimo->sig = nuevo;
        else {
            primero = nuevo;
        }
        ultimo = nuevo;
        copia = copia->sig;
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    // Completar
    if(! primero){
        Nodo* nuevo = new Nodo(elem, nullptr, nullptr);
        primero = nuevo;
        ultimo = nuevo;
        return;
    }
    Nodo* nuevo = new Nodo(elem, nullptr, primero);
    primero->ant = nuevo;
    primero = nuevo;
}

void Lista::agregarAtras(const int& elem) {
    // Completar
    if(! primero){
        Nodo* nuevo = new Nodo(elem, nullptr, nullptr);
        primero = nuevo;
        ultimo = nuevo;
        return;
    }
    Nodo* nuevo = new Nodo(elem, ultimo, nullptr);
    ultimo->sig = nuevo;
    ultimo = nuevo;
}

void Lista::eliminar(Nat i) {
    // Completar
    if(primero == ultimo){
        delete primero;
        primero = nullptr;
        ultimo = nullptr;
        return;
    }
    Nodo* quieroSacar = primero;
    int cont = i;
    while(cont){
        cont--;
        quieroSacar = quieroSacar->sig;
    }
    if(i+1 < this->longitud()){
        (quieroSacar->sig)->ant = quieroSacar->ant;
    }
    else{
        ultimo = quieroSacar->ant;
    }
    if(i){
        (quieroSacar->ant)->sig = quieroSacar->sig;
    }
    else{
        primero = quieroSacar->sig;
    }
    delete quieroSacar;
}

int Lista::longitud() const {
    // Completar
    int cont = 0;
    Nodo* actual = primero;
    while(actual){
        cont++;
        actual = actual->sig;
    }
    return cont;
}

const int& Lista::iesimo(Nat i) const {
    // Completar
    Nodo* actual = primero;
    while(i){
        i--;
        actual = actual->sig;
    }
    return int(actual->val);
}

int& Lista::iesimo(Nat i) {
    // Completar (hint: es igual a la anterior...)
    Nodo* actual = primero;
    while(i){
        i--;
        actual = actual->sig;
    }
    return actual->val;
}

void Lista::mostrar(ostream& o) {
    // Completar
}
