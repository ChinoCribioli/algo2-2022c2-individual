// Taller de Ezequiel Cribioli, LU: 702/18

template <class T>
Conjunto<T>::Conjunto() {
    // Completar
    _raiz = nullptr;
    return;
}

template <class T>
Conjunto<T>::~Conjunto() {
    // Completar
    if(! _raiz) return;
    borrarSubarbol(_raiz);
}

template <class T>
void Conjunto<T>::borrarSubarbol(Nodo* a){
    if(a->izq) borrarSubarbol(a->izq);
    if(a->der) borrarSubarbol(a->der);
    delete a;
    return;
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo* actual = _raiz;
    while(actual){
        if(actual->valor == clave) return true;
        if(actual->valor < clave) actual = actual->der;
        else actual = actual->izq;
    }
    return false;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    Nodo* actual = _raiz, *anterior = nullptr;
    while(actual){
        if(actual->valor == clave) return;
        if(actual->valor < clave){
            anterior = actual;
            actual = actual->der;
        }
        else{
            anterior = actual;
            actual = actual->izq;
        }
    }
    Nodo* nuevo = new Nodo(clave, anterior);
    if(!anterior){
        _raiz = nuevo;
        return;
    }
    if(anterior->valor < clave) anterior->der = nuevo;
    else anterior->izq = nuevo;
    return;
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    Nodo* actual = _raiz;
    while(actual){
        if(actual->valor == clave) break;
        if(actual->valor < clave) actual = actual->der;
        else actual = actual->izq;
    }
    if(!actual) return;
    if(actual == _raiz){
        if(actual->der){
            T val_siguiente = siguiente(actual->valor);
            remover(val_siguiente);
            actual->valor = val_siguiente;
        }
        else if(actual->izq){
            _raiz = actual->izq;
            _raiz->padre = nullptr;
            delete actual;
        }
        else {
            delete actual;
            _raiz = nullptr;
        }
        return;
    }

    if(esHoja(actual)){
        if((actual->padre)->izq == actual) (actual->padre)->izq = nullptr;
        else (actual->padre)->der = nullptr;
        delete actual;
        return;
    }
    Nodo* hijo, *padr;
    if(! (actual->izq)){
        padr = actual->padre;
        hijo = actual->der;
        hijo->padre = padr;
        if(padr){
            if(padr->izq == actual) padr->izq = hijo;
            else padr->der = hijo;
        }
        delete actual;
        return;
    }
    if(! (actual->der)){
        padr = actual->padre;
        hijo = actual->izq;
        hijo->padre = padr;
        if(padr){
            if(padr->izq == actual) padr->izq = hijo;
            else padr->der = hijo;
        }
        delete actual;
        return;
    }
    Nodo* siguiente = actual->der;
    while(siguiente->izq) siguiente = siguiente->izq;
    T valor_suplementario = siguiente->valor;
    remover(valor_suplementario);
    actual->valor = valor_suplementario;
    return;
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* actual = _raiz;
    while(actual){
        if(actual->valor == clave) break;
        if(actual->valor < clave) actual = actual->der;
        else actual = actual->izq;
    }
    if(actual->der){
        actual = actual->der;
        while(actual->izq) actual = actual->izq;
        return actual->valor;
    }
    while((actual->padre)->der == actual) actual = actual->padre;
    return (actual->padre)->valor;
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* actual = _raiz;
    while(actual->izq) actual = actual->izq;
    return actual->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* actual = _raiz;
    while(actual->der) actual = actual->der;
    return actual->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return contarSubarbol(_raiz);
}

template <class T>
unsigned int Conjunto<T>::contarSubarbol(Nodo* a) const {
    if(!a) return 0;
    return 1+contarSubarbol(a->izq)+contarSubarbol(a->der);
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

