template <typename T>
string_map<T>::string_map(){
    raiz = nullptr;
    _size = 0;
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
void string_map<T>::recursive_copy(Nodo* aRecibir, const Nodo* aSerCopiado){
    if(! aSerCopiado){
        return;
    }
    if(aSerCopiado->definicion) aRecibir->definicion = new T(*(aSerCopiado->definicion));
    else aRecibir->definicion = nullptr;
    aRecibir->siguientes = vector<Nodo*>(256,nullptr);
    for(int i = 0 ; i < 256 ; i++)if(aSerCopiado->siguientes[i]){
        aRecibir->siguientes[i] = new Nodo();
        recursive_copy(aRecibir->siguientes[i], aSerCopiado->siguientes[i]);
    }

}

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    // COMPLETAR
    recursive_delete(this->raiz);
    if(! d.raiz) return *this;
    this->raiz = new Nodo();
    recursive_copy(this->raiz, d.raiz);
    return *this;
}

template <typename T>
void string_map<T>::recursive_delete(Nodo* actual){
    if(! actual) return;
    Nodo* hijo;
    for(int i = 0 ; i < 256 ; i++)if(actual->siguientes[i]){
        hijo = actual->siguientes[i];
        recursive_delete(hijo);
        actual->siguientes[i] = nullptr;
    }
    delete actual->definicion;
    delete actual;
    actual = nullptr;
}

template <typename T>
string_map<T>::~string_map(){
    recursive_delete(raiz);
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    if(! raiz) raiz = new Nodo();
    Nodo* actual = raiz;
    for(int i = 0 ; i < clave.size() ; i++){
        if(actual->siguientes[int(clave[i])]) actual = actual->siguientes[int(clave[i])];
        else{
            actual->siguientes[int(clave[i])] = new Nodo();
            actual = actual->siguientes[int(clave[i])];
        }
    }
    if(actual->definicion) return *(actual->definicion);
    actual->definicion = new T();
    return *(actual->definicion);
}

template <typename T>
int string_map<T>::recursive_count(Nodo* actual, const string& clave, int ii) const{
    if(! actual) return 0;
    if(ii == clave.size()){
        if(actual->definicion) return 1;
        return 0;
    }
    if(actual->siguientes[int(clave[ii])]) return recursive_count(actual->siguientes[int(clave[ii])], clave, ii+1);
    return 0;
}

template <typename T>
int string_map<T>::count(const string& clave) const{
    return recursive_count(raiz, clave, 0);
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* actual = raiz;
    for(int i = 0 ; i < clave.size() ; i++){
        actual = actual->siguientes[int(clave[i])];
    }
    return *(actual->definicion);
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* actual = raiz;
    for(int i = 0 ; i < clave.size() ; i++){
        actual = actual->siguientes[int(clave[i])];
    }
    return *(actual->definicion);
}

template <typename T>
void string_map<T>::insert(const pair<string,T>& par) {
    string clave = par.first;
    T significado = par.second;
    if(! raiz){
        raiz = new Nodo();
    }
    Nodo* actual = raiz;
    for(int i = 0 ; i < clave.size() ; i++){
        if(actual->siguientes[int(clave[i])]) actual = actual->siguientes[int(clave[i])];
        else{
            actual->siguientes[int(clave[i])] = new Nodo();
            actual = actual->siguientes[int(clave[i])];
        }
    }
    if(! (actual->definicion)){
        _size++;
    }
    else{
        delete actual->definicion;
    }
    actual->definicion = new T(significado);
    return;
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    _size--;
    Nodo* actual = raiz, *anterior = nullptr;
    for(int i = 0 ; i < clave.size() ; i++){
        anterior = actual;
        actual = actual->siguientes[int(clave[i])];
    }
    delete actual->definicion;
    for(int i = 0 ; i < 256 ; i++)if(actual->siguientes[i]){
        actual->definicion = nullptr;
        return;
    }
    delete actual;
    if(anterior) anterior->siguientes[clave[clave.size()-1]] = nullptr;
    else raiz = nullptr;
    return;
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    if(raiz) return false;
    return true;
}